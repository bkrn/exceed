use std::fs::File;
use std::str;
use std::io::prelude::*;
use std::io::SeekFrom;
use std::io::{BufReader, BufWriter, Read, Write};
use std::io::{Cursor};
use byteorder::{BigEndian, ReadBytesExt, WriteBytesExt};
extern crate uuid;

pub struct Section {
    offset: u64,
    length: u64,
    section_type: SectionType,
}

impl Section {
    pub fn default_bytes(&self) -> Vec<u8> {
        match self.section_type {
            SectionType::UUID => uuid::Uuid::new_v4().as_bytes().to_vec(),
            SectionType::Text => vec![32u8; self.length as usize],
            _ => vec![0u8; self.length as usize],
        }
    }

    pub fn bytes_to_value(&self, data: Vec<u8>) -> SectionValue {
        match self.section_type {
            SectionType::UUID => SectionValue::UUID(uuid::Uuid::from_bytes(&data).unwrap()),
            SectionType::Flag => SectionValue::Flag(data[0]),
            SectionType::Text => SectionValue::Text(str::from_utf8(&data).unwrap().to_owned()),
            SectionType::Integer => SectionValue::Integer(Cursor::new(data).read_u64::<BigEndian>().unwrap()),
        }
    }

    pub fn value_to_bytes(value: SectionValue) -> Vec<u8> {
        match value {
            SectionValue::UUID(d) => d.as_bytes().to_vec(),
            SectionValue::Flag(d) => vec![d],
            SectionValue::Text(d) => d.as_bytes().to_vec(),
            SectionValue::Integer(d) => {
                let mut wtr = vec![];
                wtr.write_u64::<BigEndian>(d).unwrap();
                wtr
            },
        }
    }

    pub fn read_or_init(&self, mut file: &File) -> Vec<u8> {
        let _ = file.seek(SeekFrom::Start(self.offset));
        let mut data = vec![0u8; self.length as usize];
        let read_bytes;
        {
            let mut br = BufReader::new(file);
            read_bytes = br.read(&mut data).expect("Unable to read from exceed file");
        }
        if read_bytes < self.length as usize {
            let default = self.default_bytes();
            self.write_bytes(file, &&default);
            data = default;
        };
        data
    }

    pub fn read_or_init_to_value(&self, mut file: &File) -> SectionValue {
        let data = self.read_or_init(file);
        self.bytes_to_value(data)
    }

    pub fn write_value(&self, mut file: &File, data: SectionValue) {
        self.write_bytes(file, &Section::value_to_bytes(data))
    }

    pub fn write_bytes(&self, mut file: &File, data: &Vec<u8>) {
        let _ = file.seek(SeekFrom::Start(self.offset));
        let mut f = BufWriter::new(file);
        f.write_all(data).expect("Unable to write to exceed file");
    }
}

pub enum SectionType {
    Flag,
    Text,
    Integer,
    UUID,
}

pub enum SectionValue {
    Flag(u8),
    Text(String),
    Integer(u64),
    UUID(uuid::Uuid),
}

impl SectionValue {
    pub fn as_int(self) -> Option<u64> {
        match self {
            SectionValue::Integer(d) => Some(d),
            _ => None,
        }
    }
    pub fn as_string(self) -> Option<String> {
        match self {
            SectionValue::Text(d) => Some(d),
            _ => None,
        }
    }
    pub fn as_byte(self) -> Option<u8> {
        match self {
            SectionValue::Flag(d) => Some(d),
            _ => None,
        }
    }
    pub fn as_uuid(self) -> Option<uuid::Uuid> {
        match self {
            SectionValue::UUID(d) => Some(d),
            _ => None,
        }
    }
}


pub mod workbook {
    use sections::{Section, SectionType};

    pub const STATUS: Section = Section {
        length: 1,
        offset: 0,
        section_type: SectionType::Flag,
    };
    pub const IDENTITY: Section = Section {
        length: 16,
        offset: STATUS.offset + STATUS.length,
        section_type: SectionType::UUID,
    };
    pub const VERSION: Section = Section {
        length: 16,
        offset: IDENTITY.offset + IDENTITY.length,
        section_type: SectionType::UUID,
    };
    pub const TABLECOUNT: Section = Section {
        length: 8,
        offset: VERSION.offset + VERSION.length,
        section_type: SectionType::Integer,
    };
    pub const APPLICATION_VERSION: Section = Section {
        length: 8,
        offset: TABLECOUNT.offset + TABLECOUNT.length,
        section_type: SectionType::Integer,
    };
    pub const LENGTH: u64 = (
        APPLICATION_VERSION.offset + 
        APPLICATION_VERSION.length
    );
}

pub mod table {
    use sections::{Section, SectionType};

    pub const IDENTITY: Section = Section {
        length: 16,
        offset: 0,
        section_type: SectionType::UUID,
    };
    pub const VERSION: Section = Section {
        length: 16,
        offset: IDENTITY.offset + IDENTITY.length,
        section_type: SectionType::UUID,
    };
    pub const COLUMNCOUNT: Section = Section {
        length: 8,
        offset: VERSION.offset + VERSION.length,
        section_type: SectionType::Integer,
    };
    pub const NAME: Section = Section {
        length: 50,
        offset: COLUMNCOUNT.offset + COLUMNCOUNT.length,
        section_type: SectionType::Text,
    };
    pub const LENGTH: u64 = (
        NAME.offset + 
        NAME.length
    );

}

pub mod column {
    use sections::{Section, SectionType};

    pub const IDENTITY: Section = Section {
        length: 16,
        offset: 0,
        section_type: SectionType::UUID,
    };
    pub const VERSION: Section = Section {
        length: 16,
        offset: IDENTITY.offset + IDENTITY.length,
        section_type: SectionType::UUID,
    };
    pub const ROWCOUNT: Section = Section {
        length: 8,
        offset: VERSION.offset + VERSION.length,
        section_type: SectionType::Integer,
    };
    pub const NAME: Section = Section {
        length: 50,
        offset: ROWCOUNT.offset + ROWCOUNT.length,
        section_type: SectionType::Text,
    };
    pub const DATATYPE: Section = Section {
        length: 8,
        offset: NAME.offset + NAME.length,
        section_type: SectionType::Integer,
    };
    pub const DATALEN: Section = Section {
        length: 8,
        offset: DATATYPE.offset + DATATYPE.length,
        section_type: SectionType::Integer,
    };
    pub const UNIT: Section = Section {
        length: 8,
        offset: DATALEN.offset + DATALEN.length,
        section_type: SectionType::Integer,
    };

}
