pub enum RequestError {
    Unimplmented,
    Invalid,
}

impl RequestError {
    pub fn value(&self) -> &[u8] {
        match *self {
            RequestError::Unimplmented => b"Request is valid but response is unimplemented\n",
            RequestError::Invalid => b"Request is invalid and should not be repeated as written\n",
        }
    }
}