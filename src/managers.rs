use std::fs::File;
use std::io::{self, Write};
use sections;
use sections::SectionValue;
extern crate serde_json;
extern crate uuid;
use commands;
use commands::{Command, Target, Verb};
use messages::RequestError;

#[derive(Debug, Deserialize, Copy, Clone)]
pub enum SerializeOption {
    Deep,
    Shallow,
}

#[derive(Debug, Serialize)]
pub struct Children<T> {
    #[serde(default = "default_serialize_option", skip_serializing)]
    serialize_option: SerializeOption,
    objects: Vec<T>,
}

impl<T> Children<T> {
    fn skip_serialize(c: &Children<T>) -> bool {
        match c.serialize_option {
            SerializeOption::Deep => false,
            SerializeOption::Shallow => true,
        }
    }
}

#[derive(Debug, Serialize)]
pub enum Manager {
    Workbook(WorkbookManager),
    Table(TableManager),
    Column(ColumnManager),
    Row(RowManager),
}

impl Manager {
    pub fn match_id(&self, id: uuid::Uuid) -> bool {
        match self {
            &Manager::Workbook(_) => false,
            &Manager::Table(ref p) => (p.id == id),
            &Manager::Column(ref p) => (p.id == id),
            &Manager::Row(_) => false,
        }
    }

    pub fn to_json_vec(&self) -> Vec<u8> {
        match self {
            &Manager::Workbook(ref p) => (serde_json::to_vec_pretty(p).unwrap()),
            &Manager::Table(ref p) => (serde_json::to_vec_pretty(p).unwrap()),
            &Manager::Column(ref p) => (serde_json::to_vec_pretty(p).unwrap()),
            &Manager::Row(ref p) => (serde_json::to_vec_pretty(p).unwrap()),
        }
    }
}

fn default_serialize_option() -> SerializeOption {
    SerializeOption::Shallow
}

#[derive(Debug, Serialize)]
pub struct WorkbookManager {
    #[serde(skip_serializing)]
    file: File,
    status: u8,
    identity: uuid::Uuid,
    version: uuid::Uuid,
    table_count: u64,
    application_version: u64,
    #[serde(skip_serializing_if = "Children::skip_serialize")]
    children: Children<Manager>,
}

/// gets the bit at position `n`. Bits are numbered from 0 (least significant) to 31 (most significant).
fn get_bit_at(input: u64, n: u8) -> bool {
    if n < 64 {
        input & (1 << n) != 0
    } else {
        false
    }
}

impl WorkbookManager {
    fn init(&mut self) {
        self.read_file_status();
        self.read_file_identity();
        self.read_file_version();
        self.read_tables_length();
        self.read_application_version();
    }

    fn table_locations(&self) -> Vec<u64> {
        let mut locations = Vec::new();
        for i in 0..64 {
            if get_bit_at(self.table_count, i) {
                locations.push(i as u64);
            }
        }
        locations
    }

    fn read_tables_length(&mut self) {
        match sections::workbook::TABLECOUNT
            .read_or_init_to_value(&self.file)
            .as_int()
        {
            Some(d) => self.table_count = d,
            None => panic!("Can't unwrap value"),
        };
    }

    fn read_file_identity(&mut self) {
        match sections::workbook::IDENTITY
            .read_or_init_to_value(&self.file)
            .as_uuid()
        {
            Some(d) => self.identity = d,
            None => panic!("Can't unwrap value"),
        };
    }

    fn read_file_version(&mut self) {
        match sections::workbook::VERSION
            .read_or_init_to_value(&self.file)
            .as_uuid()
        {
            Some(d) => self.version = d,
            None => panic!("Can't unwrap value"),
        };
    }

    fn read_application_version(&mut self) {
        match sections::workbook::APPLICATION_VERSION
            .read_or_init_to_value(&self.file)
            .as_int()
        {
            Some(d) => {
                if d == 0 {
                    sections::workbook::APPLICATION_VERSION
                        .write_value(&self.file, SectionValue::Integer(self.application_version))
                } else if d > self.application_version {
                    panic!("File version is greater than application version")
                } else if d < self.application_version {
                    panic!("File version is less than application version")
                }
            }
            None => panic!("Can't unwrap value"),
        };
    }

    fn read_file_status(&mut self) {
        match sections::workbook::STATUS
            .read_or_init_to_value(&self.file)
            .as_byte()
        {
            Some(d) => self.status = d,
            None => panic!("Can't unwrap value"),
        };
    }

    fn get_table(&self, id: uuid::Uuid) -> Option<&Manager> {
        let mut res: Option<&Manager> = None;
        for tbl in &self.children.objects {
            if tbl.match_id(id) {
                res = Some(tbl);
                break;
            }
        }
        res
    }

    fn get_manager(&self, payload: Command) -> Option<&Manager> {
        match payload {
            Command::Table(_) => self.get_table(payload.id()),
            Command::Column(_) => self.get_table(payload.id()),
            Command::Row(_) => self.get_table(payload.id()),
        }
    }

    fn put_table(&mut self, id: uuid::Uuid, payload: commands::Table) -> Option<TableManager> {
        None
    }

    fn remove_table(&mut self, id: uuid::Uuid, payload: commands::Table) -> Option<TableManager> {
        None
    }

    fn handle_payload(&self, cmd: Command) -> Vec<u8> {
        let null = vec![110, 117, 108, 108];
        match (cmd.clone(), cmd.verb()) {
            (Command::Table(payload), Verb::PUT) => {
                let tbl = self.create_table(payload.name.unwrap());
                serde_json::to_vec_pretty(&tbl).unwrap()
            },
            (cmd, Verb::READ) => match self.get_manager(cmd) {
                None => null,
                Some(mngr) => mngr.to_json_vec(),
            },
            (Command::Table(_), Verb::REMOVE) => null,
            (Command::Column(_), Verb::PUT) => null,
            (Command::Column(_), Verb::REMOVE) => null,
            (Command::Row(_), Verb::PUT) => null,
            (Command::Row(_), Verb::REMOVE) => null,
        }
    }

    pub fn handle(&self, trgt: Target, payload: Option<Command>) {
        match (trgt, payload) {
            (Target::Workbook, _) | (Target::NoTarget, _) => {
                //The workbook is immutable to the outside world
                //So just run the read without doing anything else
                io::stdout()
                    .write(&serde_json::to_vec_pretty(self).unwrap())
                    .unwrap();
            }
            (_, Some(p)) => {
                match p.is_valid() {
                    (false, msg) => {
                        //If the payload is per se invalid short circut
                        //Meaning if we're missing required fields
                        //Checking for invalid fields happens later
                        io::stdout().write(msg.as_bytes()).unwrap();
                    }
                    (true, _) => {
                        let resp = self.handle_payload(p);
                        io::stdout().write(&resp).unwrap();
                    }
                }
            }
            (_, _) => {
                io::stderr().write(RequestError::Invalid.value()).unwrap();
            }
        };
    }

    fn create_table(&self, name: String) -> TableManager {
        let mngr = TableManager{
            file: self.file.try_clone().unwrap(),
            id: uuid::Uuid::new_v4(),
            version: uuid::Uuid::new_v4(),
            column_count: 0,
            name: name.as_bytes().to_vec(),
            children: Children{
                objects: Vec::new(),
                serialize_option: default_serialize_option()
            }
        };
        mngr
    }
}

#[derive(Debug, Serialize)]
pub struct TableManager {
    #[serde(skip_serializing)]
    file: File,
    id: uuid::Uuid,
    version: uuid::Uuid,
    column_count: u64,
    #[serde(serialize_with = "as_str")]
    name: Vec<u8>,
    #[serde(skip_serializing_if = "Children::skip_serialize")]
    children: Children<ColumnManager>,
}

#[derive(Debug, Serialize)]
pub struct ColumnManager {
    #[serde(skip_serializing)]
    file: File,
    id: uuid::Uuid,
    version: uuid::Uuid,
    row_count: u64,
    data_len: u64,
    data_type: u8,
    name: Vec<u8>,
    #[serde(skip_serializing_if = "Children::skip_serialize")]
    children: Children<RowManager>,
}

#[derive(Debug, Serialize)]
pub struct RowManager {
    #[serde(skip_serializing)]
    file: File,
    indexes: Vec<u64>,
    data: Vec<u8>,
}

pub fn new_manager(file: File, app_version: u64) -> WorkbookManager {
    let mut rdr = WorkbookManager {
        file: file,
        status: 0,
        identity: uuid::Uuid::nil(),
        version: uuid::Uuid::nil(),
        table_count: 0,
        children: Children {
            serialize_option: default_serialize_option(),
            objects: Vec::with_capacity(10),
        },
        application_version: app_version,
    };
    rdr.init();
    rdr
}
