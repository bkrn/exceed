use std::fs::File;
use std::fs::OpenOptions;
use std::path::Path;
mod sections;
mod managers;
mod commands;
mod messages;

#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate clap;

extern crate serde_json;

extern crate uuid;
extern crate byteorder;

fn get_file(name: &str) -> File {
    let file = OpenOptions::new()
        .read(true)
        .write(true)
        .create(true)
        .open(name)
        .expect("Could not open file");
    file
}

fn get_target(matches: &clap::ArgMatches) -> commands::Target {
    let trgt: commands::Target;
    match value_t!(matches, "TARGET", commands::Target) {
        Ok(t) => trgt = t,
        Err(e) => {
            match e.kind {
                clap::ErrorKind::ArgumentNotFound => trgt = commands::Target::NoTarget,
                _ => e.exit(),
            }
        },
    };
    trgt
}

fn get_string_payload(matches: &clap::ArgMatches, trgt: commands::Target ) -> Option<commands::Command> {
    match (matches.value_of("PAYLOAD"), trgt) {
        (Some(d), commands::Target::Table) => {let v: commands::Table = serde_json::from_str(d).unwrap(); Some(commands::Command::Table(v))},
        (Some(d), commands::Target::Column) => {let v: commands::Column = serde_json::from_str(d).unwrap(); Some(commands::Command::Column(v))},
        (Some(d), commands::Target::Row) => {let v: commands::Row = serde_json::from_str(d).unwrap(); Some(commands::Command::Row(v))},
        (_, _) => None,
    }
}


fn main() {
    let matches = clap_app!(myapp =>
        (version: "0.0")
        (author: "Aaron D. <aaron@bkrn.org>")
        (about: "Spreadsheets for humans & machines")
        (@arg FILE: +required "Sets the exceed file to create or use")
        (@arg TARGET: -t +takes_value "Identify the target level for the payload")
        (@arg PAYLOAD: -p +takes_value requires[TARGET] "Sets the level of debugging information")
        (@arg debug: -d ... "Sets the level of debugging information")
    ).get_matches();

    // Calling .unwrap() is safe here because "FILE" is required
    let filename = matches.value_of("FILE").unwrap();
    let file = get_file(filename);
    //let pipelock = get_pipelock(filename);
    let manager = managers::new_manager(file, 1);

    let trgt = get_target(&matches);
    let payload = get_string_payload(&matches, trgt);

    manager.handle(trgt, payload);
}