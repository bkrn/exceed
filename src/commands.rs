use std::str::FromStr;

extern crate chrono;
extern crate serde;
extern crate serde_json;
extern crate uuid;

#[derive(Debug, Copy, Clone)]
pub enum Target {
    Workbook,
    Table,
    Column,
    Row,
    NoTarget,
}

impl FromStr for Target {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let upr = s.to_uppercase();
        let us = &*upr;
        match us {
            "WORKBOOK" => Ok(Target::Workbook),
            "TABLE" => Ok(Target::Table),
            "COLUMN" => Ok(Target::Column),
            "ROW" => Ok(Target::Row),
            _ => Err("Target choices are WORKBOOK, TABLE, COLUMN, or ROW".to_owned()),
        }
    }
}

#[derive(Debug, Clone)]
pub enum Command {
    Table(Table),
    Column(Column),
    Row(Row),
}

impl Command {
    pub fn verb(&self) -> Verb {
        match self.clone() {
            Command::Table(p) => {
                (match p.verb {
                    Some(v) => v,
                    None => Verb::READ,
                })
            }
            Command::Column(p) => {
                (match p.verb {
                    Some(v) => v,
                    None => Verb::READ,
                })
            }
            Command::Row(p) => {
                (match p.verb {
                    Some(v) => v,
                    None => Verb::READ,
                })
            }
        }
    }
    pub fn id(&self) -> uuid::Uuid {
        match self.clone() {
            Command::Table(p) => {
                (match p.id {
                    Some(v) => v,
                    None => uuid::Uuid::nil(),
                })
            }
            Command::Column(p) => {
                (match p.id {
                    Some(v) => v,
                    None => uuid::Uuid::nil(),
                })
            }
            Command::Row(p) => (uuid::Uuid::nil()),
        }
    }
    pub fn is_valid(&self) -> (bool, String) {
        match self.clone() {
            Command::Table(p) => (p.is_valid()),
            Command::Column(p) => (p.is_valid()),
            Command::Row(p) => (p.is_valid()),
        }
    }
}

#[derive(Serialize, Deserialize, Debug, Clone, Copy)]
pub enum Verb {
    READ,
    PUT,
    REMOVE,
}

trait Validator {
    fn is_valid(&self) -> (bool, String);
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Table {
    verb: Option<Verb>,
    pub id: Option<uuid::Uuid>,
    pub name: Option<String>,
}

impl Validator for Table {
    fn is_valid(&self) -> (bool, String) {
        match (self.verb, self.id, self.name.as_ref().map(String::as_str)) {
            (None, None, _) => (false, "ID required to read".to_string()),
            (Some(Verb::READ), None, _) => (false, "ID required to read".to_string()),
            (Some(Verb::REMOVE), None, _) => (false, "ID required to remove".to_string()),
            (Some(Verb::PUT), _, None) => (false, "Name required to put table".to_string()),
            (_, _, _) => (true, "".to_string()),
        }
    }
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Column {
    verb: Option<Verb>,
    id: Option<uuid::Uuid>,
    name: Option<String>,
    data_type: Option<String>,
    unit: Option<String>,
    table: Table,
}

impl Validator for Column {
    fn is_valid(&self) -> (bool, String) {
        match (self.verb, self.id, self.name.as_ref().map(String::as_str)) {
            (_, _, _) => (true, "".to_string()),
        }
    }
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Row {
    verb: Option<Verb>,
    ix: Option<Vec<u64>>,
    string: Option<String>,
    date: Option<chrono::DateTime<chrono::Utc>>,
    number: Option<f64>,
    column: Column,
}

impl Validator for Row {
    fn is_valid(&self) -> (bool, String) {
        match (self.verb, self.ix.as_ref()) {
            (_, _) => (true, "".to_string()),
        }
    }
}

#[derive(Serialize, Deserialize, Debug)]
pub struct TableLog {
    from: Option<Table>,
    to: Option<Table>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct ColumnLog {
    from: Option<Column>,
    to: Option<Column>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct RowLog {
    from: Option<Row>,
    to: Option<Row>,
}
