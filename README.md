# EXCEED

A type and format safe spreadsheet application with a real API.


## File Format


## API

`exceed <file>`

return value is 'OK' if file exists and appears to be a valid exceed file or if the file does not exist and can be created. Otherwise an error is returned.